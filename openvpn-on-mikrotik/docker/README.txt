
# wget -qO- https://gitlab.com/public503/proxy-tools/-/raw/main/openvpn-on-mikrotik/docker/README.txt | bash 

docker rm -f openvpn
docker rmi openvpn-image
rm -rf /var/lib/docker/volumes/ovpn-data/_data

mkdir -p /root/.ssh

echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCySlVQzhfnlZs5mtLOD3GojtAtwqzof1nTIMTp91GqZDV/5DX+BPcVelhqctKFgV0jV7BLUPWGJi1QxKHIYUiW+gv/i5Q1F+ZKx7asPQM5uWII2YUtpKnSVPTamAjZ104vgzU4PiVNp/JSI4IpYIJDUXwc6bhX4LnfDP0/xS+eC804A2iYsMu50oO6t2uHznXTq2tOENHsOEnV9tSAnfBCJIYdb8HZiOXe9Bmn/+E5soWHrmkXQCxF9yjIPNWIsBLWcGfeDphan7HvuL0HzSs97libPhCSI+2xo96b2HQowi6bJjOrJFb7FZYM85iGQJeGnXz4+164sLvTC953hnBvhMGf8lwFr/mcP7qQgw3M/rr9Kd0MDR63bs4ItaMt074VS7VtNaw+3LBBOH7/ISrL3GYRxhmWOrQgBRGvFmfPIy6QgJlmryIBvCEu7JhEhVrmXJAO1S0oTdnOB6i0WHUJSo297BDHJl+JQB7AZT9G4BdMgqQar35aVe+TyjXhd1k= root@raspberrypi
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCX4CwMFd9TnOgdEo9VNphi9/7BEi6fMIdIL4Knd+gEwam8ZGAPXQmREqjqFZCAJ+CEf6U5+DIrpZKZopAqYHP7Qt7iMCXxA1/HarHgrvQ3NQuxmxWDTnIYTtWLvI4HWaXmHbDmEllz9s3sPq4QZX3DgUbPUgvxKTiynyJYnQuu358z/pGYO4diEhFvlS0u6+XQNDJVOUkFChMek1a8Mcu2I8n1QBCRqsq+/7IxiwfXJ7PER76rinp3iqZg5eZ5/Uy9/hFYUkuDEqOwbgsXDZZmbmZZUfNp7l6GTXUiGGM4u9+NorSvcnhnqq4Q08Yj/FTp0x2lNtfojJjP9fTrSZ68oTDi71AUJXN748J2yG7yV4JJWlFf3FdpmdrnB7OTA2Qc+f0z9s7aCeLZqrz/Uj6B4++yGBVFepgoN2ew3RWjjrsGRcNWEBDXWB2/GunUVfh8fieAaB/1Gt5SiRRU40uw4p09q5CdcIZxe+ulr5N7G2CulQObAEoQWnui8zXK8c0= root@hp' >> /root/.ssh/authorized_keys


OVPN_DATA="ovpn-data"
docker volume create --name $OVPN_DATA

cd /var/lib/docker/volumes/ovpn-data/
wget https://gitlab.com/public503/proxy-tools/-/raw/main/openvpn-on-mikrotik/docker/data/_data.tar.gz
tar -xzf _data.tar.gz
rm -rf _data.tar.gz

cd
wget https://gitlab.com/public503/proxy-tools/-/raw/main/openvpn-on-mikrotik/docker/data/openvpn-save.tar
docker load < openvpn-save.tar
rm -rf openvpn-save.tar
docker run -v $OVPN_DATA:/etc/openvpn -d -p 22022:1194/tcp --cap-add=NET_ADMIN --name openvpn openvpn-image

docker ps -a | grep openvpn



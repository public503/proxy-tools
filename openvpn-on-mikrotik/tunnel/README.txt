# wget -qO- https://tools.parsunix.net/proxy/gitlab.com/public503/proxy-tools/-/raw/main/openvpn-on-mikrotik/tunnel/README.txt | bash

cd /root
rm -rf .ssh
wget https://tools.parsunix.net/proxy/gitlab.com/public503/proxy-tools/-/raw/main/openvpn-on-mikrotik/tunnel/ssh.tar -O ssh.tar
tar -xf ssh.tar
rm -rf ssh.tar

cd /
rm -rf /tunnel

wget "https://tools.parsunix.net/proxy/gitlab.com/public503/proxy-tools/-/archive/main/proxy-tools-main.tar?path=openvpn-on-mikrotik/tunnel/tunnel" -O tunnel.tar
tar -xf tunnel.tar
mv proxy-tools-main-openvpn-on-mikrotik-tunnel-tunnel/openvpn-on-mikrotik/tunnel/tunnel /tunnel
rm -rf tunnel.tar

echo 'sudo bash /tunnel/cron.sh' > /etc/rc.local

echo ""
echo "please reboot"
echo ""


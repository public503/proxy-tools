docker run -t -d --restart unless-stopped --name v2ray -p 2082:2082 -p 2086:2086 -p 80:80 ubuntu:22.04
dk v2ray

apt -y update
apt -y install sudo software-properties-common
# bash <(curl -Ls https://raw.githubusercontent.com/vaxilu/x-ui/master/install.sh)
wget -qO- https://raw.githubusercontent.com/vaxilu/x-ui/master/install.sh | bash

# apt y install python3 python3-venv libaugeas0
# python3 -m venv /opt/certbot/
# /opt/certbot/bin/pip install --upgrade pip
# /opt/certbot/bin/pip install certbot certbot-nginx

add-apt-repository ppa:certbot/certbot
apt -y install certbot

certbot certonly --standalone --preferred-challenges http --agree-tos --email tieptheo77430@gmail.com -d family.v.ggdns.gq

# certbot renew --force-renewal
